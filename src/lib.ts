import { parse, compileScript } from 'vue/compiler-sfc';
import MagicString from 'magic-string';
import type { TransformResult } from 'vite';
import type { Options } from './index';
// id: F:/**/**/example/vite_vue/src/App.vue
export function transformScript({ code = '', id = '' }, options: Options): TransformResult {
  const FILENAME_RE = /.*\/(\S*)/;
  let s: MagicString | undefined;
  const str = () => s || (s = new MagicString(code));
  const { descriptor } = parse(code);
  const { hasMap = true, hires = true } = options;

  if (!descriptor.script && descriptor.scriptSetup) {
    const result = compileScript(descriptor, { id });
    const name = result.attrs.name;
    const lang = result.attrs.lang;
    const inheritAttrs = result.attrs.inheritAttrs ?? true;

    if (name) {
      str().appendLeft(
        0,
        `
<script ${lang ? `lang="${lang}"` : ''}>
  import { defineComponent } from 'vue'
  export default defineComponent({
  name: '${name}',
  inheritAttrs: ${inheritAttrs},
})
</script>\n`,
      );
    }

    let map = null;
    if (hasMap) {
      const filename = FILENAME_RE.exec(id)![1];
      map = str().generateMap({ hires });
      map.file = filename;
      map.sources = [filename];
    }

    return {
      // 跳过SourceMap类型检测
      map: map as null,
      code: str().toString(),
    };
  } else {
    return {
      code,
      map: null,
    };
  }
}
